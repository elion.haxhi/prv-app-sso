package prv.app.sso.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import prv.app.sso.repository.ZRepository;
import prv.app.sso.entity.ZEntity;

@Service
@Slf4j
public class Z {

    private ZRepository zRepository;

    public Z(ZRepository zRepository){
        this.zRepository = zRepository;
    }

    public ZEntity updateValue(Long id, String v) {
        log.info("Start update service");
        ZEntity zEntity = zRepository.findById(id).orElseThrow(RuntimeException::new);

        Integer value = Integer.parseInt(v);
        zEntity.setValue(value);
        log.info("End update service");

        return zRepository.save(zEntity);
    }
}
