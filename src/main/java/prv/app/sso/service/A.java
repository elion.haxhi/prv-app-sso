package prv.app.sso.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class A {

    private Z delegate;

    public A(Z delegate){
        this.delegate = delegate;
    }


    @Transactional //if you comment @Transactional the test pass
    public Integer testValue(long id, String v) {
        return delegate.updateValue(id,v).getValue();
    }
}
