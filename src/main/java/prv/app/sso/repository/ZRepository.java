package prv.app.sso.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import prv.app.sso.entity.ZEntity;

@Repository
@Transactional(readOnly = true)
public interface ZRepository extends JpaRepository<ZEntity,Long>{

    ZEntity findByValue(Integer v);
}
