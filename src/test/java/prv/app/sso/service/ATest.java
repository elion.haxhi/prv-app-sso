package prv.app.sso.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class ATest {

    @Autowired
    A a;

    @Test
    public void _2shouldThrowaNumberFormatException(){
        assertThatExceptionOfType(NumberFormatException.class).isThrownBy(() -> a.testValue(1, "101a"));
    }

    @Test
    public void shouldReturnAnInteger(){
        assertEquals((Integer) 10000, a.testValue(1,"10000"));
    }

}